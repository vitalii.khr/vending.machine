<?php

namespace App\Http\Controllers;

use App\Machine\Builder\SnackMachineBuilder;
use App\Machine\Purchase\Transaction;
use App\Machine\Stock\SlotIndex;
use App\Money\Banknote;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MachineController
{
    public function __construct()
    {
        $this->machine = (new SnackMachineBuilder())
            ->size(2, 2)
            ->acceptBanknotes()
            ->availableCoinsForChange()
            ->build();
    }

    public function loadMachine(): Response
    {
        return new JsonResponse([
            'stock' => $this->machine->getSlots(),
            'accepted_money' => $this->machine->getAcceptedMoney(),
            'available_change' => $this->machine->getAvailableChange(),
        ]);
    }

    public function purchase(Request $request): Response
    {
        $slotIndexArray = explode(':', $request->get('slot'));

        $transaction = new Transaction(
            new SlotIndex($slotIndexArray[0], $slotIndexArray[1]),
            (int)$request->get('quantity'),
            Banknote::EUR((float)$request->get('paid')),
        );

        $purchase = $this->machine->execute($transaction);

        return new JsonResponse([
            'message' => 'Successful purchase.',
            'purchase' => $purchase
        ]);
    }
}
