<?php

declare(strict_types=1);

namespace App\Command;

use App\Command\ConsoleFormatter\PurchaseChangeToConsoleTableFormatter;
use App\Machine\MachineInterface;
use App\Machine\Purchase\PurchaseInterface;
use App\Machine\Purchase\Transaction;
use App\Machine\Stock\SlotIndex;
use App\Money\Banknote;
use App\Money\Money;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function array_search;
use function preg_match;
use function range;
use function sprintf;
use function strtolower;

final class PurchaseCommand extends Command
{
    private MachineInterface $machine;

    public function __construct(string $name = null, MachineInterface $machine)
    {
        parent::__construct($name);

        $this->machine = $machine;
    }

    protected function configure(): void
    {
        $this
            ->setName('machine:purchase')
            ->setDescription('Purchase a snack in a vending machine.')
            ->setDefinition(
                new InputDefinition([
                    new InputArgument('slot', InputArgument::REQUIRED, 'Please enter a snack you want to buy.'),
                    new InputArgument('quantity', InputArgument::REQUIRED, 'Quantity is required.'),
                    new InputArgument('paid_amount', InputArgument::REQUIRED, 'Paid amount is required.'),
                ]),
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $slotPosition = $input->getArgument('slot');

        preg_match('#^(\d+)(\w+)#', $slotPosition, $matched);
        $rowIndex = ((int)$matched[1]) - 1;
        $colIndex = $this->convertAlphaColumnToInteger($matched[2]);

        $transaction = new Transaction(
            new SlotIndex($rowIndex, $colIndex),
            (int)$input->getArgument('quantity'),
            Banknote::EUR((float)$input->getArgument('paid_amount')),
        );

        $purchase = $this->machine->execute($transaction);

        $output->writeln($this->formatConfirmationMessage($purchase));

        $table = new Table($output);
        $table = (new PurchaseChangeToConsoleTableFormatter($purchase->getChange()))->format($table);
        $table->render();

        return 0;
    }

    private function convertAlphaColumnToInteger(string $alpha): int
    {
        return array_search(
            strtolower($alpha),
            range('a', 'z'),
        );
    }

    private function formatConfirmationMessage(PurchaseInterface $purchase): string
    {
        if ($purchase->getTransaction()->getQuantity() > 1) {
            return sprintf(
                "You bought %d items of %s for %s, each for %s.",
                $purchase->getTransaction()->getQuantity(),
                $purchase->getProduct()->getName(),
                Money::EUR($purchase->getTransaction()->getQuantity() * $purchase->getProduct()->getPrice()->getValue()),
                $purchase->getProduct()->getPrice()
            );
        }

        return sprintf(
            "You bought %d item of %s for %s.",
            $purchase->getTransaction()->getQuantity(),
            $purchase->getProduct()->getName(),
            $purchase->getProduct()->getPrice()
        );
    }
}
