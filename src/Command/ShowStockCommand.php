<?php

declare(strict_types=1);

namespace App\Command;

use App\Command\ConsoleFormatter\MachineStockToConsoleTableFormatter;
use App\Machine\MachineInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ShowStockCommand extends Command
{
    private MachineInterface $machine;

    public function __construct(string $name = null, MachineInterface $machine = null)
    {
        parent::__construct($name);
        $this->machine = $machine;
    }

    protected function configure(): void
    {
        $this
            ->setName('machine:show-stock')
            ->setDescription('Show all snacks.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        (new MachineStockToConsoleTableFormatter($this->machine))
            ->format(new Table($output))
            ->render();

        return 0;
    }
}
