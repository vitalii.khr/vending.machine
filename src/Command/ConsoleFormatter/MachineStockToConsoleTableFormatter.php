<?php

declare(strict_types=1);

namespace App\Command\ConsoleFormatter;

use App\Machine\MachineInterface;
use App\Machine\Stock\SlotInterface;
use App\Money\Money;
use Symfony\Component\Console\Helper\Table;

use function assert;
use function count;
use function range;
use function sprintf;

final class MachineStockToConsoleTableFormatter
{
    private MachineInterface $machine;

    public function __construct(MachineInterface $machine)
    {
        $this->machine = $machine;
    }

    public function format(Table $table): Table
    {
        $rows = [];
        $maxColumnCount = 0;

        foreach ($this->machine->getSlots() as $rowIndex => $rowWithSlots) {
            $row = [$rowIndex + 1];

            foreach ($rowWithSlots as $slot) {
                assert($slot instanceof SlotInterface);
                $row[] = $slot->hasProduct()
                    ? sprintf(
                        '%s - %s (%d items)',
                        $slot->getProduct()->getName(),
                        $slot->getProduct()->getPrice(),
                        $slot->getQuantity(),
                    )
                    : '-';
            }

            $rows[] = $row;

            if ($maxColumnCount < count($rowWithSlots)) {
                $maxColumnCount = count($rowWithSlots);
            }
        }

        $table->setHeaders(
            $this->generateHeaders($maxColumnCount),
        );

        $table->setRows($rows);

        return $table;
    }

    private function generateHeaders(int $maxColumnCount): array
    {
        $alphabet = range('a', 'z');
        $headers = [''];

        for ($i = 0; $i < $maxColumnCount; $i++) {
            $headers[] = $alphabet[$i];
        }

        return $headers;
    }
}
