<?php

declare(strict_types=1);

namespace App\Command\ConsoleFormatter;

use App\Machine\Change\ChangeInterface;
use App\Machine\Change\ChangeItem;
use App\Money\Money;
use Symfony\Component\Console\Helper\Table;

use function assert;

final class PurchaseChangeToConsoleTableFormatter
{
    private ChangeInterface $change;

    public function __construct(ChangeInterface $change)
    {
        $this->change = $change;
    }

    public function format(Table $table): Table
    {
        $rows = [];

        foreach ($this->change->get() as $changeItem) {
            assert($changeItem instanceof ChangeItem);
            $rows[] = [
                $changeItem->getMoney(),
                $changeItem->getQuantity(),
            ];
        }

        if ($this->change->getLeftAmount() > 0) {
            $rows[] = [
                \sprintf('We could not return you this amount due to technical reason.'),
                Money::EUR($this->change->getLeftAmount()),
            ];
        }

        $table->setHeaders(['Coins', 'Count']);
        $table->setRows($rows);

        return $table;
    }
}
