<?php

declare(strict_types=1);

namespace App\Machine\Change;

use App\Money\Money;
use InvalidArgumentException;

class Change implements ChangeInterface, \JsonSerializable
{
    private array $change = [];
    private float $leftAmount = 0;

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->change;
    }

    /**
     * @param  Money  $money
     * @param  int  $quantity
     * @return void
     */
    public function add(Money $money, int $quantity): void
    {
        $this->change[] = new ChangeItem($money, $quantity);
    }

    /**
     * @return float
     */
    public function getLeftAmount(): float
    {
        return $this->leftAmount;
    }

    /**
     * @param  float  $amount
     * @return void
     */
    public function setLeftAmount(float $amount): void
    {
        if ($amount < 0) {
            throw new InvalidArgumentException('Invalid amount. It must be greater than or equal to 0.');
        }

        $this->leftAmount = $amount;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'left_amount' => $this->getLeftAmount(),
            'change' => $this->get(),
        ];
    }
}
