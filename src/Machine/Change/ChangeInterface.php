<?php

declare(strict_types=1);

namespace App\Machine\Change;

use App\Money\Money;

interface ChangeInterface
{
    public function get(): array;

    public function add(Money $money, int $quantity): void;

    public function getLeftAmount(): float;

    public function setLeftAmount(float $amount): void;
}
