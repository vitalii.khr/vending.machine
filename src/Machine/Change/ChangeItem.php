<?php

declare(strict_types=1);

namespace App\Machine\Change;

use App\Money\Money;
use InvalidArgumentException;

class ChangeItem implements \JsonSerializable
{
    private Money $money;
    private int $quantity;

    /**
     * @param  Money  $money
     * @param  int  $quantity
     */
    public function __construct(Money $money, int $quantity)
    {
        if ($quantity < 1) {
            throw new InvalidArgumentException('Invalid quantity. It must be a positive integer.');
        }

        $this->money = $money;
        $this->quantity = $quantity;
    }

    /**
     * @return Money
     */
    public function getMoney(): Money
    {
        return $this->money;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function jsonSerialize(): array
    {
        return [
            'money' => $this->getMoney(),
            'quantity' => $this->getQuantity(),
        ];
    }
}
