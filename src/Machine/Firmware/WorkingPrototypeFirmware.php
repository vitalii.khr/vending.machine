<?php

declare(strict_types=1);

namespace App\Machine\Firmware;

use App\Machine\Change\Change;
use App\Machine\Change\ChangeInterface;
use App\Money\Money;

final class WorkingPrototypeFirmware implements FirmwareInterface
{
    /**
     * @param  Money  $money
     * @param  array  $acceptedMoney
     * @return bool
     */
    public function acceptMoney(Money $money, array $acceptedMoney): bool
    {
        foreach ($acceptedMoney as $validMoney) {
            if ($money == $validMoney) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param  float  $paid
     * @param  float  $price
     * @param  array  $availableChange
     * @return ChangeInterface
     */
    public function calculateChange(float $paid, float $price, array $availableChange): ChangeInterface
    {
        $sortedAvailableChange = $this->sortMoneyByDesc($availableChange);

        $change = new Change();

        if ($price > $paid or $price === $paid) {
            return $change;
        }

        $amountInCentsLeft = intval(($paid * 100) - ($price * 100));

        /** @var Money $changeItem */
        foreach ($sortedAvailableChange as $changeItem) {
            $itemValueInCents = intval($changeItem->getValue() * 100);

            $quantity = intdiv($amountInCentsLeft, $itemValueInCents);

            if ($quantity > 0) {
                $amountInCentsLeft = $amountInCentsLeft % $itemValueInCents;
                $change->add($changeItem, $quantity);
            }
        }

        if ($amountInCentsLeft > 0) {
            $change->setLeftAmount($amountInCentsLeft / 100);
        }

        return $change;
    }

    /**
     * @param  array  $array
     * @return array
     */
    private function sortMoneyByDesc(array $array): array
    {
        $sorted = $array;

        usort($sorted, function (Money $a, Money $b) {
            if ($a->getValue() === $b->getValue()) {
                return 0;
            }

            return $a->getValue() > $b->getValue() ? -1 : 1;
        });

        return $sorted;
    }
}
