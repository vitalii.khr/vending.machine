<?php

declare(strict_types=1);

namespace App\Machine\Firmware;

use App\Machine\Change\ChangeInterface;
use App\Money\Money;

interface FirmwareInterface
{
    public function acceptMoney(Money $money, array $acceptedMoney): bool;

    public function calculateChange(float $paid, float $price, array $availableChange): ChangeInterface;
}
