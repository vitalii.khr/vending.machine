<?php

declare(strict_types=1);

namespace App\Machine\Purchase;

use App\Machine\Stock\SlotIndex;
use App\Money\Money;

interface TransactionInterface
{
    public function getSlotIndex(): SlotIndex;

    public function getQuantity(): int;

    public function getPaidMoney(): Money;
}
