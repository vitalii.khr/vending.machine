<?php

declare(strict_types=1);

namespace App\Machine\Purchase;

use App\Machine\Stock\SlotIndex;
use App\Money\Money;

final class Transaction implements TransactionInterface, \JsonSerializable
{
    private SlotIndex $slotIndex;
    private int $quantity;
    private Money $paidMoney;

    /**
     * @param  SlotIndex  $slotIndex
     * @param  int  $quantity
     * @param  Money  $paidMoney
     */
    public function __construct(SlotIndex $slotIndex, int $quantity, Money $paidMoney)
    {
        $this->slotIndex = $slotIndex;
        $this->quantity = $quantity;
        $this->paidMoney = $paidMoney;
    }

    /**
     * @return SlotIndex
     */
    public function getSlotIndex(): SlotIndex
    {
        return $this->slotIndex;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return Money
     */
    public function getPaidMoney(): Money
    {
        return $this->paidMoney;
    }

    public function jsonSerialize(): array
    {
        return [
            'slot_index' => $this->getSlotIndex(),
            'quantity' => $this->getQuantity(),
            'paid_money' => $this->getPaidMoney(),
        ];
    }
}
