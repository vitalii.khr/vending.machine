<?php

declare(strict_types=1);

namespace App\Machine\Purchase;

use App\Machine\Change\ChangeInterface;
use App\Product\ProductInterface;

interface PurchaseInterface
{
    public function getProduct(): ProductInterface;

    public function getTransaction(): TransactionInterface;

    public function getChange(): ChangeInterface;
}
