<?php

declare(strict_types=1);

namespace App\Machine\Purchase;

use App\Machine\Change\ChangeInterface;
use App\Product\ProductInterface;

final class Purchase implements PurchaseInterface, \JsonSerializable
{
    private TransactionInterface $transaction;
    private ProductInterface $product;
    private ChangeInterface $change;

    /**
     * @param  TransactionInterface  $transaction
     * @param  ProductInterface  $product
     * @param  ChangeInterface  $change
     */
    public function __construct(TransactionInterface $transaction, ProductInterface $product, ChangeInterface $change)
    {
        $this->transaction = $transaction;
        $this->product = $product;
        $this->change = $change;
    }

    /**
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    /**
     * @return TransactionInterface
     */
    public function getTransaction(): TransactionInterface
    {
        return $this->transaction;
    }

    /**
     * @return ChangeInterface
     */
    public function getChange(): ChangeInterface
    {
        return $this->change;
    }

    public function jsonSerialize(): array
    {
        return  [
          'product' => $this->getProduct(),
          'change' => $this->getChange(),
          'transaction' => $this->getTransaction(),
        ];
    }
}
