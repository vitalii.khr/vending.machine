<?php

declare(strict_types=1);

namespace App\Machine;

use App\Machine\Purchase\PurchaseInterface;
use App\Machine\Purchase\TransactionInterface;

interface MachineInterface
{
    public function execute(TransactionInterface $purchaseTransaction): PurchaseInterface;

    public function getSlots(): array;

    public function getAcceptedMoney(): array;

    public function getAvailableChange(): array;
}
