<?php

declare(strict_types=1);

namespace App\Machine\Builder;

use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\MachineInterface;
use App\Machine\SnackMachine;
use App\Machine\Stock\OneSizedSlotStock;
use App\Machine\Stock\SlotIndex;
use App\Money\Banknote;
use App\Money\Coin;
use App\Money\Money;
use App\Product\Snack;

class SnackMachineBuilder
{
    private int $rows;
    private int $columns;
    private array $acceptedMoney;
    private array $availableChange;

    /**
     * @param  int  $rows
     * @param  int  $columns
     * @return $this
     */
    public function size(int $rows, int $columns): self
    {
        $this->rows = $rows;
        $this->columns = $columns;

        return $this;
    }

    /**
     * @param  array  $values
     * @return $this
     */
    public function acceptBanknotes(array $values = [5, 10, 20, 50]): self
    {
        foreach ($values as $value) {
            $this->acceptedMoney[] = Banknote::EUR($value);
        }

        return $this;
    }

    /**
     * @param  array  $values
     * @return $this
     */
    public function availableCoinsForChange(array $values = [0.01, 0.02, 0.05, 0.10, 0.20, 1.00, 2.00]): self
    {
        foreach ($values as $value) {
            $this->availableChange[] = Coin::EUR($value);
        }

        return $this;
    }

    /**
     * @return MachineInterface
     */
    public function build(): MachineInterface
    {
        $stock = new OneSizedSlotStock($this->rows, $this->columns);

        $stock->addProduct(
            new SlotIndex(0, 0),
            new Snack('Twix', Money::EUR(1.55)),
            10
        );

        $stock->addProduct(
            new SlotIndex(0, 1),
            new Snack('Mars', Money::EUR(1.89)),
            10
        );

        $stock->addProduct(
            new SlotIndex(1, 0),
            new Snack('Nuts', Money::EUR(1.99)),
            50
        );

        $stock->addProduct(
            new SlotIndex(1, 1),
            new Snack('Bounty', Money::EUR(2)),
            10
        );

        return new SnackMachine(
            $stock,
            new WorkingPrototypeFirmware(),
            $this->acceptedMoney,
            $this->availableChange
        );
    }
}
