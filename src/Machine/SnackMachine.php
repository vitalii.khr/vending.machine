<?php

declare(strict_types=1);

namespace App\Machine;

use App\Machine\Firmware\FirmwareInterface;
use App\Machine\Purchase\Purchase;
use App\Machine\Purchase\PurchaseInterface;
use App\Machine\Purchase\TransactionInterface as PurchaseTransactionInterface;
use App\Machine\Stock\StockInterface;
use RuntimeException;

use function implode;
use function sprintf;

final class SnackMachine implements MachineInterface
{
    private StockInterface $stock;
    private FirmwareInterface $firmware;
    private array $acceptedMoney;
    private array $availableChange;

    /**
     * @param  StockInterface  $stock
     * @param  FirmwareInterface  $firmware
     * @param  array  $acceptedMoney
     * @param  array  $availableChange
     */
    public function __construct(
        StockInterface $stock,
        FirmwareInterface $firmware,
        array $acceptedMoney,
        array $availableChange
    ) {
        $this->stock = $stock;
        $this->firmware = $firmware;
        $this->acceptedMoney = $acceptedMoney;
        $this->availableChange = $availableChange;
    }

    /**
     * @param  PurchaseTransactionInterface  $purchaseTransaction
     * @return PurchaseInterface
     */
    public function execute(PurchaseTransactionInterface $purchaseTransaction): PurchaseInterface
    {
        $product = $this->stock->getProduct($purchaseTransaction->getSlotIndex());

        $totalPrice = $product->getPrice()->getValue() * $purchaseTransaction->getQuantity();

        if ($totalPrice > $purchaseTransaction->getPaidMoney()->getValue()) {
            throw new RuntimeException(sprintf('Funds are not enough. Total price is %s.', $totalPrice));
        }

        if (! $this->firmware->acceptMoney($purchaseTransaction->getPaidMoney(), $this->acceptedMoney)) {
            throw new RuntimeException(
                sprintf(
                    'Money are not supportable. This machine supports only: %s',
                    implode(', ', $this->acceptedMoney)
                )
            );
        }

        $change = $this->firmware->calculateChange(
            $purchaseTransaction->getPaidMoney()->getValue(),
            $totalPrice,
            $this->availableChange
        );

        $this->stock->deductProduct(
            $purchaseTransaction->getSlotIndex(),
            $purchaseTransaction->getQuantity(),
        );

        return new Purchase($purchaseTransaction, $product, $change);
    }

    /**
     * @return array
     */
    public function getSlots(): array
    {
        return $this->stock->getSlots();
    }

    /**
     * @return array
     */
    public function getAcceptedMoney(): array
    {
        return $this->acceptedMoney;
    }

    /**
     * @return array
     */
    public function getAvailableChange(): array
    {
        return $this->availableChange;
    }
}
