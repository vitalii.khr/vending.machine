<?php

declare(strict_types=1);

namespace App\Machine\Stock;

use App\Product\ProductInterface;
use InvalidArgumentException;
use RuntimeException;

use function sprintf;

final class Slot implements SlotInterface, \JsonSerializable
{
    private ?ProductInterface $product;
    private int $quantity = 0;

    /**
     * @return null|ProductInterface
     */
    public function getProduct(): ?ProductInterface
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param  ProductInterface  $product
     * @param  int  $quantity
     * @return void
     */
    public function addProduct(ProductInterface $product, int $quantity = 1): void
    {
        if ($this->hasProduct()) {
            throw new RuntimeException('The slot already contains a product.');
        }

        if ($quantity < 1) {
            throw new InvalidArgumentException('Invalid quantity. It must be a positive integer.');
        }

        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @param  int  $quantity
     * @return void
     */
    public function deduct(int $quantity = 1): void
    {
        if ($quantity < 1) {
            throw new InvalidArgumentException('Invalid quantity. It must be a positive integer.');
        }

        if (! $this->hasProduct()) {
            throw new RuntimeException('Slot is empty.');
        }

        if ($quantity > $this->quantity) {
            throw new RuntimeException(
                sprintf(
                    'You are trying to get %s item(s). Slot contains %d item(s). You can get maximum %d item(s)',
                    $quantity,
                    $this->quantity,
                    $this->quantity
                )
            );
        }

        $this->quantity -= $quantity;

        if ($this->quantity === 0) {
            $this->product = null;
        }
    }

    /**
     * @return bool
     */
    public function hasProduct(): bool
    {
        return isset($this->product);
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'product' => $this->getProduct(),
            'quantity' => $this->getQuantity(),
        ];
    }
}
