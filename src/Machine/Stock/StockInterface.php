<?php

declare(strict_types=1);

namespace App\Machine\Stock;

use App\Product\ProductInterface;

interface StockInterface
{
    public function getSlots(): array;

    public function addProduct(SlotIndex $slotIndex, ProductInterface $product, int $quantity = 1): void;

    public function deductProduct(SlotIndex $slotIndex, int $quantity = 1): void;

    public function getProduct(SlotIndex $slotIndex): ProductInterface;

    public function findSlot(SlotIndex $slotIndex): SlotInterface;

    public function getRows(): int;

    public function getColumns(): int;
}
