<?php

declare(strict_types=1);

namespace App\Machine\Stock;

use App\Product\ProductInterface;
use InvalidArgumentException;
use RuntimeException;

final class OneSizedSlotStock implements StockInterface
{
    private int $rows;
    private int $columns;
    private array $slots;

    /**
     * @param  int  $rows
     * @param  int  $columns
     */
    public function __construct(int $rows, int $columns)
    {
        if ($rows < 1 or $columns < 1) {
            throw new RuntimeException('Rows and columns must be at least 1.');
        }

        $this->rows = $rows;
        $this->columns = $columns;

        $this->initSlots();
    }

    /**
     * @return array
     */
    public function getSlots(): array
    {
        return $this->slots;
    }

    /**
     * @param  SlotIndex  $slotIndex
     * @param  ProductInterface  $product
     * @param  int  $quantity
     * @return void
     */
    public function addProduct(SlotIndex $slotIndex, ProductInterface $product, int $quantity = 1): void
    {
        $this
            ->findSlot($slotIndex)
            ->addProduct($product, $quantity);
    }

    /**
     * @param  SlotIndex  $slotIndex
     * @return ProductInterface
     */
    public function getProduct(SlotIndex $slotIndex): ProductInterface
    {
        return $this->findSlot($slotIndex)->getProduct();
    }

    /**
     * @param  SlotIndex  $slotIndex
     * @param  int  $quantity
     * @return void
     */
    public function deductProduct(SlotIndex $slotIndex, int $quantity = 1): void
    {
        $this
            ->findSlot($slotIndex)
            ->deduct($quantity);
    }

    /**
     * @param  SlotIndex  $slotIndex
     * @return SlotInterface
     */
    public function findSlot(SlotIndex $slotIndex): SlotInterface
    {
        if (
            array_key_exists($slotIndex->getRowIndex(), $this->slots)
            and array_key_exists($slotIndex->getColumnIndex(), $this->slots[$slotIndex->getRowIndex()])
        ) {
            return $this->slots[$slotIndex->getRowIndex()][$slotIndex->getColumnIndex()];
        }

        throw new InvalidArgumentException('Cannot find slot.');
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getColumns(): int
    {
        return $this->columns;
    }

    /**
     * @return void
     */
    private function initSlots(): void
    {
        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $this->slots[$i][$j] = new Slot();
            }
        }
    }
}
