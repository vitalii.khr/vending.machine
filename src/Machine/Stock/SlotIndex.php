<?php

declare(strict_types=1);

namespace App\Machine\Stock;

final class SlotIndex implements \JsonSerializable
{
    private int $rowIndex;
    private int $columnIndex;

    /**
     * @param  int  $rowIndex
     * @param  int  $columnIndex
     */
    public function __construct(int $rowIndex, int $columnIndex)
    {
        $this->rowIndex = $rowIndex;
        $this->columnIndex = $columnIndex;
    }

    /**
     * @return int
     */
    public function getRowIndex(): int
    {
        return $this->rowIndex;
    }

    /**
     * @return int
     */
    public function getColumnIndex(): int
    {
        return $this->columnIndex;
    }

    public function jsonSerialize(): array
    {
        return [
           'row_index' => $this->getRowIndex(),
           'column_index' => $this->getColumnIndex(),
        ];
    }
}
