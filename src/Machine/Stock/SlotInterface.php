<?php

declare(strict_types=1);

namespace App\Machine\Stock;

use App\Product\ProductInterface;

interface SlotInterface
{
    public function getProduct(): ?ProductInterface;

    public function getQuantity(): int;

    public function addProduct(ProductInterface $product, int $quantity = 1): void;

    public function deduct(int $quantity = 1): void;

    public function hasProduct(): bool;
}
