<?php

namespace App\Money;

use InvalidArgumentException;

class Money implements \JsonSerializable
{
    private float $value;
    private string $currency;

    /**
     * @param  float  $value
     * @return static
     */
    public static function EUR(float $value): static
    {
        return new static($value, '€');
    }

    /**
     * @param  float  $value
     * @param  string  $currency
     */
    public function __construct(float $value, string $currency)
    {
        if ($value <= 0) {
            throw new InvalidArgumentException('Value must be a positive.');
        }

        $this->value = (float) number_format($value, 2);
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return number_format($this->value, 2).$this->currency;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }


    public function jsonSerialize(): array
    {
        return [
            'value' => $this->getValue(),
            'formatted' => (string) $this,
            'currency' => $this->getCurrency(),
            'type' => strtolower(basename(str_replace('\\', '/', get_class($this)))),
        ];
    }
}
