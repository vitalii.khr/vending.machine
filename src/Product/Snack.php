<?php

declare(strict_types=1);

namespace App\Product;

use App\Money\Money;

final class Snack implements ProductInterface, \JsonSerializable
{
    private string $name;
    private Money $price;

    /**
     * @param  string  $name
     * @param  Money  $price
     */
    public function __construct(string $name, Money $price)
    {
        if (mb_strlen($name) < 1) {
            throw new \InvalidArgumentException('Name length must be more than 0.');
        }

        if ($price->getValue() < 0) {
            throw new \InvalidArgumentException('Price must be more than or equal to 0.');
        }

        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'name' => $this->getName(),
            'price' => $this->getPrice(),
        ];
    }
}
