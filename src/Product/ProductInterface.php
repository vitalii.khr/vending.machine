<?php

declare(strict_types=1);

namespace App\Product;

use App\Money\Money;

interface ProductInterface
{
    public function getName(): string;

    public function getPrice(): Money;
}
