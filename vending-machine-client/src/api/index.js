import axios from 'axios';

export default {

  loadMachine() {
    return axios.get(`/load-machine`);
  },

  purchase(slot, quantity, paid) {
    return axios.postForm(`/purchase`, {
      slot,
      quantity,
      paid,
    });
  },

};