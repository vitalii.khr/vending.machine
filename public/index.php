<?php

require_once __DIR__."./../vendor/autoload.php";

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$request = Request::createFromGlobals();

$context = new RequestContext();
$context->fromRequest($request);

$loadMachine = new Route('/load-machine', [
    'controller' => 'App\\Http\\Controllers\\MachineController',
    'method' => 'loadMachine',
]);
$purchase = new Route('/purchase', [
    'controller' => 'App\\Http\\Controllers\\MachineController',
    'method' => 'purchase',
]);
$purchase->setMethods(['POST']);

$routes = new RouteCollection();
$routes->add('load-machine', $loadMachine);
$routes->add('purchase', $purchase);

$matcher = new UrlMatcher($routes, $context);

try {
    $matched = $matcher->match($request->getPathInfo());
    $controller = new $matched['controller']();
    $response = $controller->{$matched['method']}($request);
} catch (ResourceNotFoundException $exception) {
    $response = new JsonResponse(['message' => 'Route not found.'], 404);
} catch (\Exception $exception) {
    $response = new JsonResponse(['message' => $exception->getMessage()], 500);
}

$response->send();
