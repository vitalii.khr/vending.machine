<?php

namespace Tests\Functional\Command;

use App\Command\ShowStockCommand;
use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\SnackMachine;
use App\Machine\Stock\OneSizedSlotStock;
use App\Machine\Stock\SlotIndex;
use App\Money\Money;
use App\Product\Snack;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ShowStockCommandTest extends TestCase
{
	public function test_show_stock()
	{
		$snack1 = new Snack('Twix', Money::EUR(11.50));
		$snack2 = new Snack('MozartCandy', Money::EUR(12.50));
		$snack3 = new Snack('M&Ms', Money::EUR(13.50));
		$snack4 = new Snack('Cola', Money::EUR(14.50));

		$stock = new OneSizedSlotStock(3, 4);
		$stock->addProduct(
			new SlotIndex(0, 0),
			$snack1,
			11
		);
		$stock->addProduct(
			new SlotIndex(0, 1),
			$snack2,
			12
		);
		$stock->addProduct(
			new SlotIndex(1, 1),
			$snack3,
			13
		);
		$stock->addProduct(
			new SlotIndex(1, 0),
			$snack4,
			14
		);

		$machine = new SnackMachine(
			$stock,
			new WorkingPrototypeFirmware(), [],
			[]
		);

		$commandTester = new CommandTester(new ShowStockCommand('name', $machine));
		$commandTester->execute([]);
		$commandTester->assertCommandIsSuccessful();

		$output = $commandTester->getDisplay();
		$this->assertStringContainsString('Twix - 11.50€ (11 items)', $output);
		$this->assertStringContainsString('MozartCandy - 12.50€ (12 items)', $output);
		$this->assertStringContainsString('M&Ms - 13.50€ (13 items)', $output);
		$this->assertStringContainsString('Cola - 14.50€ (14 items)', $output);
	}
}