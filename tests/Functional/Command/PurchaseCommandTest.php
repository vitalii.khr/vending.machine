<?php

namespace Tests\Functional\Command;

use App\Command\PurchaseCommand;
use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\SnackMachine;
use App\Machine\Stock\OneSizedSlotStock;
use App\Machine\Stock\SlotIndex;
use App\Money\Banknote;
use App\Money\Coin;
use App\Money\Money;
use App\Product\Snack;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PurchaseCommandTest extends TestCase
{
    public function test_purchase_one_snack()
    {
        $snack1 = new Snack('Twix', Money::EUR(11.50));
        $snack2 = new Snack('MozartCandy', Money::EUR(12.50));
        $snack3 = new Snack('M&Ms', Money::EUR(13.50));
        $snack4 = new Snack('Cola', Money::EUR(14.50));

        $stock = new OneSizedSlotStock(3, 4);
        $stock->addProduct(
            new SlotIndex(0, 0),
            $snack1,
            11
        );
        $stock->addProduct(
            new SlotIndex(0, 1),
            $snack2,
            12
        );
        $stock->addProduct(
            new SlotIndex(1, 1),
            $snack3,
            13
        );
        $stock->addProduct(
            new SlotIndex(1, 0),
            $snack4,
            14
        );

        $machine = new SnackMachine(
            $stock,
            new WorkingPrototypeFirmware(),
            [Banknote::EUR(10), Banknote::EUR(20)],
            [Coin::EUR(2)]
        );

        $commandTester = new CommandTester(new PurchaseCommand('name', $machine));
        $commandTester->execute([
            'slot' => '1a',
            'quantity' => 1,
            'paid_amount' => 20,
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('You bought 1 item of Twix for 11.50€', $output);
        $this->assertStringContainsString('We could not return you this amount due to technical reason. | 0.50€', $output);
    }

    public function test_purchase_three_snack_items()
    {
        $snack1 = new Snack('Twix', Money::EUR(11.50));
        $snack2 = new Snack('MozartCandy', Money::EUR(12.50));
        $snack3 = new Snack('M&Ms', Money::EUR(13.50));
        $snack4 = new Snack('Cola', Money::EUR(14.50));

        $stock = new OneSizedSlotStock(3, 4);
        $stock->addProduct(
            new SlotIndex(0, 0),
            $snack1,
            11
        );
        $stock->addProduct(
            new SlotIndex(0, 1),
            $snack2,
            12
        );
        $stock->addProduct(
            new SlotIndex(1, 0),
            $snack3,
            13
        );
        $stock->addProduct(
            new SlotIndex(1, 1),
            $snack4,
            14
        );

        $buyQuantity = 3;

        $machine = new SnackMachine(
            $stock,
            new WorkingPrototypeFirmware(),
            [Banknote::EUR(10), Banknote::EUR(50)],
            [Coin::EUR(0.01)]
        );

        $commandTester = new CommandTester(new PurchaseCommand('name', $machine));
        $commandTester->execute([
            'slot' => '2b',
            'quantity' => $buyQuantity,
            'paid_amount' => 50,
        ]);
        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();

        $this->assertStringContainsStringIgnoringCase("You bought $buyQuantity items of {$snack4->getName()}", $output);
        $this->assertStringContainsStringIgnoringCase("0.01€ | 650", $output);
        $this->assertStringNotContainsStringIgnoringCase('We could not return you this amount due to technical reason', $output);
    }
}