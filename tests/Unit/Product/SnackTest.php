<?php

declare(strict_types = 1);

namespace Tests\Unit\Product;

use App\Money\Money;
use App\Product\Snack;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class SnackTest extends TestCase
{
	/**
	 * @dataProvider validSnackProvider
	 */
	public function test_valid_data_should_create_snack($name, $price): void
	{
		$snack = new Snack($name, $price);

		$this->assertSame($name, $snack->getName());
		$this->assertSame($price, $snack->getPrice());
	}

	/**
	 * @dataProvider invalidPriceProvider
	 */
	public function test_invalid_price_should_throw_exception($name, $price, $exceptionClass): void
	{
		$this->expectException($exceptionClass);

		$snack = new Snack($name, $price);
	}

	/**
	 * @dataProvider invalidNameProvider
	 */
	public function test_invalid_name_should_throw_exception($name, $price, $exceptionClass): void
	{
		$this->expectException($exceptionClass);

		$snack = new Snack($name, $price);
	}

	/**
	 * @see test_valid_snack_should_create_object
	 * @return array[]
	 */
	public function validSnackProvider(): array
	{
		return [
			['Mars', Money::EUR(1.19)],
			['Chips', Money::EUR(100)],
			['Snikers', Money::EUR(100.001)],
			['Nuts', Money::EUR(100.001)],
		];
	}

	/**
	 * @see test_invalid_price_should_throw_exception
	 * @return array
	 */
	public function invalidPriceProvider(): array
	{
		return [
			['Nuts', 'invalid', TypeError::class],
		];
	}

	/**
	 * @see test_invalid_name_should_throw_exception
	 * @return array
	 */
	public function invalidNameProvider(): array
	{
		return [
			['', Money::EUR(1), InvalidArgumentException::class],
			[null, Money::EUR(123), TypeError::class],
		];
	}
}