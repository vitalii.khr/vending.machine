<?php

namespace Tests\Unit\Machine\Stock;

use App\Machine\Stock\Slot;
use App\Money\Money;
use App\Product\ProductInterface;
use App\Product\Snack;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class SlotTest extends TestCase
{

	/**
	 * @dataProvider addProductToSlotProvider
	 */
	public function test_add_product_to_slot($product, $quantity): void
	{
		$slot = new Slot();
		$slot->addProduct($product, $quantity);

		$this->assertSame($product, $slot->getProduct());
		$this->assertSame($quantity, $slot->getQuantity());
	}

	/**
	 * @dataProvider validDeductProductFromSlotProvider
	 */
	public function test_valid_deduct_product_from_slot($product, $quantity, $deduct, $expected): void
	{
		$slot = new Slot();
		$slot->addProduct($product, $quantity);
		$slot->deduct($deduct);

		$this->assertSame($expected, $slot->getQuantity());
	}

	/**
	 * @dataProvider invalidDeductProductFromSlotProvider
	 */
	public function test_invalid_deduct_product_from_slot($product, $quantity, $deduct): void
	{
		$this->expectException(RuntimeException::class);

		$slot = new Slot();
		$slot->addProduct($product, $quantity);
		$slot->deduct($deduct);
	}

	public function test_deduct_invalid_quantity_product_from_slot(): void
	{
		$this->expectException(InvalidArgumentException::class);

		$slot = new Slot();
		$slot->addProduct($this->createProduct(), 1);
		$slot->deduct(0);
	}

	public function test_deduct_from_empty_slot(): void
	{
		$this->expectException(RuntimeException::class);

		$slot = new Slot();
		$slot->addProduct($this->createProduct(), 1);
		$slot->deduct(1);
		$slot->deduct(1);
	}

	public function addProductToSlotProvider(): array
	{
		return [
			[new Snack('Snack name #1', Money::EUR(10)), 15],
			[new Snack('Snack name #2', Money::EUR(20)), 1],
			[new Snack('Snack name #3', Money::EUR(0.50)), 10],
			[new Snack('Snack name #4', Money::EUR(0.01)), 150],
			[new Snack('Snack name #5', Money::EUR(15)), 18],
		];
	}

	public function validDeductProductFromSlotProvider(): array
	{
		return [
			[new Snack('Snack name #1', Money::EUR(10)), 15, 2, 13],
			[new Snack('Snack name #2', Money::EUR(20)), 1, 1, 0],
			[new Snack('Snack name #3', Money::EUR(0.50)), 10, 5, 5],
			[new Snack('Snack name #4', Money::EUR(0.01)), 150, 50, 100],
			[new Snack('Snack name #5', Money::EUR(15)), 18, 5, 13],
		];
	}

	public function invalidDeductProductFromSlotProvider(): array
	{
		return [
			[new Snack('Snack name #1', Money::EUR(10)), 15, 17],
			[new Snack('Snack name #2', Money::EUR(20)), 1, 2],
			[new Snack('Snack name #3', Money::EUR(0.50)), 10, 11],
			[new Snack('Snack name #4', Money::EUR(0.01)), 150, 200],
			[new Snack('Snack name #5', Money::EUR(15)), 1, 2],
		];
	}

	private function createProduct(): ProductInterface
	{
		return new Snack('Snack name #'.rand(1,10), Money::EUR(10));
	}

}