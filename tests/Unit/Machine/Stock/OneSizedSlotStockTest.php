<?php

namespace Tests\Unit\Machine\Stock;

use App\Machine\Stock\OneSizedSlotStock;
use App\Machine\Stock\SlotIndex;
use App\Money\Money;
use App\Product\Snack;
use InvalidArgumentException;
use RuntimeException;
use PHPUnit\Framework\TestCase;

class OneSizedSlotStockTest extends TestCase
{

	/**
	 * @dataProvider constructorDataProvider
	 */
	public function test_constructor(int $rows, int $columns): void
	{
		$stock = new OneSizedSlotStock($rows, $columns);

		$this->assertEquals($rows, $stock->getRows());
		$this->assertEquals($rows, count($stock->getSlots()));
		$this->assertEquals($columns, $stock->getColumns());
		$this->assertIsArray($stock->getSlots());
	}

	public function test_add_product(): void
	{
		$slotIndex = new SlotIndex(0,1);
		$stock = new OneSizedSlotStock(1, 2);
		$product = new Snack('Snack', Money::EUR(10));

		$stock->addProduct($slotIndex, $product, 20);
		$this->assertTrue($stock->getProduct($slotIndex) == $product);
	}

	public function test_add_product_to_invalid_slot_should_throw_exception(): void
	{
		$this->expectException(InvalidArgumentException::class);
		$slotIndex = new SlotIndex(10,1);
		$stock = new OneSizedSlotStock(1, 2);
		$product = new Snack('Snack', Money::EUR(10));

		$stock->addProduct($slotIndex, $product, 20);
	}

	public function test_deduct_product(): void
	{
		$slotIndex = new SlotIndex(0,1);
		$stock = new OneSizedSlotStock(1, 2);
		$product = new Snack('Snack', Money::EUR(10));

		$stock->addProduct($slotIndex, $product, 20);
		$this->assertTrue($stock->getProduct($slotIndex) == $product);

		$stock->deductProduct($slotIndex, 10);
		$this->assertSame(10, $stock->findSlot($slotIndex)->getQuantity());

		$stock->deductProduct($slotIndex, 9);
		$this->assertSame(1, $stock->findSlot($slotIndex)->getQuantity());

		$stock->deductProduct($slotIndex, 1);
		$this->assertSame(0, $stock->findSlot($slotIndex)->getQuantity());
	}

	public function test_deduct_more_than_exists_should_throw_exception(): void
	{
		$this->expectException(RuntimeException::class);
		$slotIndex = new SlotIndex(0,1);
		$stock = new OneSizedSlotStock(1, 2);
		$product = new Snack('Snack', Money::EUR(10));

		$stock->addProduct($slotIndex, $product, 20);
		$this->assertTrue($stock->getProduct($slotIndex) == $product);

		$stock->deductProduct($slotIndex, 21);
	}

	public function constructorDataProvider(): array
	{
		return [
			[1, 1],
			[2, 1],
			[2, 2],
			[3, 1],
			[3, 2],
			[3, 3],
		];
	}

}