<?php

namespace Tests\Unit\Machine\Firmware;

use App\Machine\Change\ChangeItem;
use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Money\Banknote;
use App\Money\Coin;
use App\Money\Money;
use PHPUnit\Framework\TestCase;

class WorkingPrototypeFirmwareTest extends TestCase
{
	/**
	 * @dataProvider calculateChangeProvider
	 */
	public function test_calculate_change(float $paid, float $price, array $availableChange, array $usedChange, float $leftAmount): void
	{
		$firmware = new WorkingPrototypeFirmware();

		$change = $firmware->calculateChange($paid, $price, $availableChange);

		$this->assertEquals($leftAmount, $change->getLeftAmount());

		$diff = array_udiff(
			$change->get(), $usedChange,
			function ($obj_a, $obj_b) {
				return $obj_a == $obj_b ? 0 : -1;
			}
		);

		$this->assertIsArray($diff);
		$this->assertEquals($diff, []);
	}

	/**
	 * @dataProvider validAcceptMoneyProvider
	 */
	public function test_accept_money(Money $money, array $validArray): void
	{
		$firmware = new WorkingPrototypeFirmware();

		$this->assertTrue($firmware->acceptMoney($money, $validArray));
	}

	/**
	 * @dataProvider invalidAcceptMoneyProvider
	 */
	public function test_invalid_money_shouldnt_accept(Money $money, array $validArray): void
	{
		$firmware = new WorkingPrototypeFirmware();

		$this->assertFalse($firmware->acceptMoney($money, $validArray));
	}

	public function calculateChangeProvider(): array
	{
		return [
			[
				20,
				10,
				[],
				[],
				10,
			],
			[
				10,
				10,
				[Coin::EUR(1)],
				[],
				0,
			],
			[
				10,
				15,
				[Coin::EUR(1)],
				[],
				0,
			],
			[
				50,
				20.50,
				[Coin::EUR(1)],
				[new ChangeItem(Coin::EUR(1), 29)],
				0.50,
			],
			[
				50,
				20.50,
				[Coin::EUR(1), Coin::EUR(0.20)],
				[
					new ChangeItem(Coin::EUR(1), 29),
					new ChangeItem(Coin::EUR(0.20), 2),
				],
				0.10,
			],
		];
	}

	public function validAcceptMoneyProvider(): array
	{
		return [
			[
				Banknote::EUR(5),
				[Banknote::EUR(10), Banknote::EUR(5)]
			],
			[
				Coin::EUR(2.00),
				[Banknote::EUR(10), Banknote::EUR(5), Coin::EUR(2.00)]
			],
		];
	}

	public function invalidAcceptMoneyProvider(): array
	{
		return [
			[
				Banknote::EUR(50),
				[Banknote::EUR(10), Banknote::EUR(5)]
			],
			[
				Coin::EUR(1.00),
				[Banknote::EUR(10), Banknote::EUR(5), Coin::EUR(2.00)]
			],
		];
	}
}