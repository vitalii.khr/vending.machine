<?php

declare(strict_types = 1);

namespace Tests\Unit\Money;

use App\Money\Money;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class MoneyTest extends TestCase
{
	/**
	 * @dataProvider validMoneyProvider
	 */
	public function test_valid_data_should_create_object(float $value, string $currency): void
	{
		$money = new Money($value, $currency);

		$this->assertSame($value, $money->getValue());
		$this->assertSame($currency, $money->getCurrency());
	}

	/**
	 * @dataProvider invalidValueProvider
	 */
	public function test_invalid_value_should_throw_exception($value, $currency, $exceptionClass): void
	{
		$this->expectException($exceptionClass);

		$money = new Money($value, $currency);
	}

	public function validMoneyProvider(): array
	{
		return [
			[1, '$'],
			[10, '$'],
			[100, '$'],
			[0.50, '$'],
		];
	}

	public function invalidValueProvider(): array
	{
		return [
			['invalid', '$', TypeError::class],
			[0, '$', InvalidArgumentException::class],
			[-10, '$', InvalidArgumentException::class],
		];
	}

}