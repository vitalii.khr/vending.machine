#!/usr/bin/env php
<?php
declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

use App\Command\PurchaseCommand;
use App\Command\ShowStockCommand;
use App\Machine\Builder\SnackMachineBuilder;
use Symfony\Component\Console\Application;

$application = new Application();

$machine = (new SnackMachineBuilder())
	->size(2, 2)
	->acceptBanknotes()
	->availableCoinsForChange()
	->build();

$application->addCommands([
	new ShowStockCommand('machine:show-stock', $machine),
	new PurchaseCommand('machine:purchase', $machine),
]);

$application->run();
