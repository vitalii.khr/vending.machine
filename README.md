Vending Machine
=

Simple configurable Vending Machine. 
- Stock size is configurable. 
- Configurable money validation. You can configure an array of accepted money of banknotes and coins.
- Configurable a change. You can configure an array of available change units (banknotes, coins). 
- Amount which cannot be returned (e.g. lack of coins/banknotes, etc.) will be displayed.

System requirements:
- Docker Compose

### Setup
```
$ docker-compose run --rm composer install
```



### Show stock
```
$ docker-compose run --rm php bin/console machine:show-stock
+---+-------------------------+---------------------------+
|   | a                       | b                         |
+---+-------------------------+---------------------------+
| 1 | Twix - 1.55€ (10 items) | Mars - 1.89€ (10 items)   |
| 2 | Nuts - 1.99€ (50 items) | Bounty - 2.00€ (10 items) |
+---+-------------------------+---------------------------+
```

### Purchase a snack
```
$ docker-compose run --rm php bin/console machine:purchase 1a 10 20.00
You bought 10 items of Twix for 15.50€, each for 1.55€.
+-------+-------+
| Coins | Count |
+-------+-------+
| 2.00€ | 2     |
| 0.20€ | 2     |
| 0.10€ | 1     |
+-------+-------+
```

### Run tests
```
$ docker-compose run --rm php ./vendor/bin/phpunit tests
```

### Run nginx for handling endpoints
```
$ docker-compose up -d nginx
```

### Run web client
```
$ npm run serve
```
